import React from 'react';
import './BoardSquare.css';

const BoardSquare = (props) => { 
    return(
        <div className="board-sqaure" id={props.id} onClick={props.onClick}>
            {props.children}
        </div>
    )
}

export default BoardSquare;