import React, { Component } from 'react';
import './App.css';

import GameBoard from './components/GameBoard';
import BoardSquare from './components/BoardSquare';
import CheckWinner from './CheckWinner';

class App extends Component {
  constructor(props){
    super(props);
      this.state = {
        board: {
          gameboardTiles: [],
          player1: [],
          player2: []
        },  
        currentTurn: {
          isPlayer1: true,
        },
        showWinner: false,
      }
  }

  componentDidMount(){
    this.initGameboard();
  }

  initGameboard = () => {  
    const boardSize = 9;
    const tempState = [];

    for(let i = 0; i < boardSize; i++) {
      tempState.push(<BoardSquare key={i} onClick={this.handlePlay} id={i} />);
    }
    
    this.setState({
      board: {
        ...this.state.board,
        gameboardTiles: tempState,
      }
    });
  }

  //Resets the game to the original state
  handleReset = () => {
   
    this.setState({
        board: {
          gameboardTiles: [],
          player1: [],
          player2: []
        },
        currentTurn: {
          isPlayer1: true
        },
        showWinner: false
    }, () => {
        this.initGameboard()
    })
  }

  //Updates the board view and checks for a winner
  updateBoard = (position, piece, playerBoard) => {
    let tempState = [];
   
    tempState = 
          <BoardSquare key={position}  id={position}>
              <i className={piece}></i>
            </BoardSquare>;

    const newArray = this.state.board.gameboardTiles.slice(0, Number(position))
      .concat([tempState])
        .concat(this.state.board.gameboardTiles.slice(Number(position)+1))
    
    this.setState({
      board: {
        ...this.state.board,
          gameboardTiles: newArray,      
      }
    }, ()=> {CheckWinner(playerBoard, () => {this.setState({showWinner: true})})})
    
  }

  //handles the play of the game 
  handlePlay = (e) => {

    const currentTurn = this.state.currentTurn.isPlayer1;
    let boardPiece = '';
    let position = e.target.id;

    if(currentTurn) {
       
      boardPiece = 'fas fa-times';
    
      const player1 = [...this.state.board.player1, Number(e.target.id)]
      
      this.setState({
        board:{
          ...this.state.board,
            player1,
        },
        currentTurn:{
          isPlayer1: false
        }
      }, () => {
        this.updateBoard(position, boardPiece, this.state.board.player1)
      })
    }
    else{
      boardPiece = 'far fa-circle';
    
      const player2 = [...this.state.board.player2, Number(e.target.id)]
      
      this.setState({
        board:{
          ...this.state.board,
            player2,
        },
        currentTurn:{
          isPlayer1: true
        }
      }, () => {
        this.updateBoard(position, boardPiece, this.state.board.player2)
      })
    }
  }
  
  render() {
    const player = this.state.currentTurn.isPlayer1 ? 'O ' : 'X ';
    const winnerState = this.state.showWinner ? 
    <div className="winner-dialog">{player} has won the game.
      <button className="play-again-btn" onClick={this.handleReset}>Play Again</button>
    </div> : '';
    return (
      <div className="App">
        <header className="App-header">
        {winnerState}
          <GameBoard>
            {this.state.board.gameboardTiles}
          </GameBoard>
        </header>
      </div>
    );
  }
}

export default App;
