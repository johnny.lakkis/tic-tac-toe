import React from 'react';
import './GameBoard.css';

const GameBoard = (props) => {
    return(
        <div className='game-board'>
            {props.children}
        </div>
    )
}

export default GameBoard;